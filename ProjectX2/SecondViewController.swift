//
//  SecondViewController.swift
//  ProjectX2
//
//  Created by Wes Glover on 2016-12-07.
//  Copyright © 2016 Mitchell Emery. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    @IBOutlet weak var questionLabel: UILabel!
    
    var recievedQuestion: String = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        questionLabel.text = recievedQuestion
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
